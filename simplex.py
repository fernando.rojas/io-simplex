import sys
import copy

def imprimirMat(matriz):
    for i in range(0, len(matriz)):
        for j in range(0, len(matriz[i])):
            print("%8.2f" % matriz[i][j], end=" ")
        print("")
    
    print("")

def imprimirMatGranM(matriz,posicion_variables_agregadas):
    for i in range(0, len(matriz)):
        for j in range(0, len(matriz[i])):
            if i == 0:
                if j < posicion_variables_agregadas-1:
                    if matriz[i][j][0] == 0.0:
                        print("%.3f" % (matriz[i][j][1]), end="  ")
                    elif matriz[i][j][1] == 0.0:
                        print("%.2fM" % (matriz[i][j][0]), end="  ")
                    elif matriz[i][j][1] < 0:
                        print("%.2fM%.3f" % (matriz[i][j][0], matriz[i][j][1]), end="  ")
                    else:
                        print("%.2fM-%.3f" % (matriz[i][j][0], matriz[i][j][1]), end="  ")
                elif j >= posicion_variables_agregadas:
                    if matriz[i][j][0] == 0.0:
                        print("%.3f" % (matriz[i][j][1]), end="  ")
                    elif matriz[i][j][1] == 0.0:
                        print("%.2fM" % (matriz[i][j][0]), end="  ")
                    else:
                        print("%.2fM-%.3f" % (matriz[i][j][0], matriz[i][j][1]), end="  ")
                else:
                    m = j
            elif j < posicion_variables_agregadas-1:
                print("%8.3f" % matriz[i][j], end="  ")
            elif j >= posicion_variables_agregadas:
                print("%8.3f" % matriz[i][j], end="  ")
            else:
                m = j
        if i == 0:
            if matriz[i][m][0] == 0.0:
                print("%.3f" % (matriz[i][m][1]), end="  ")
            elif matriz[i][m][1] == 0.0:
                print("%.2fM" % (matriz[i][m][0]), end="  ")
            else:
                print("%.2fM-%.3f" % (matriz[i][m][0], matriz[i][m][1]), end="  ")
        else:
            print("%8.3f" % matriz[i][m], end="  ")
        print("")

def dividirFila(fila, escalar):
    '''
    divide los valores de una fila entre un escalar
    '''
    for i in range(0,len(fila)):
        fila[i] = fila[i]/escalar
    
    return fila


def multiplicarFila(fila, escalar):
    '''
    multiplica los valores de una fila entre un escalar
    '''
    nfila = []
    for i in range(0,len(fila)):
        nfila.append(fila[i]*escalar)
    
    return nfila


def restarFilas(fila, fila2):
    '''
    resta dos filas
    '''
    nfila = []
    for i in range(0,len(fila)):
        nfila.append(fila[i]-fila2[i])
    
    return nfila


def sumarFilas(fila, fila2):
    '''
    suma dos filas
    '''
    nfila = []
    for i in range(0,len(fila)):
        nfila.append(fila[i]+fila2[i])
    
    return nfila


def copiarMatriz(matriz):
    '''
    copia una matriz
    '''
    nmatriz = []
    for i in range(0,len(matriz)):
        nfila = []
        for j in range(0,len(matriz[i])):
            nfila.append(matriz[i][j])
        nmatriz.append(nfila)

    return nmatriz


def solucionNoAcotada(matriz):
    for i in range(0, len(matriz[0])):
        columnaNoValida = True
        for j in range(0, len(matriz)):
            if matriz[j][i] > 0:
                columnaNoValida = False

        if columnaNoValida:
            return columnaNoValida

    return columnaNoValida


def metodoSimplex(encabezado, matriz):
    #Igualar a 0 la funcion objetivo
    if 'max' in encabezado:
        matriz[0] = multiplicarFila(matriz[0], -1)

    i = 1
    while i < len(matriz):
        if '<=' in matriz[i]:
            indice = matriz[i].index('<=')
            matriz[i].insert(indice,1.0) 
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0.0) 
                j += 1

            matriz[i].remove('<=')

        if '=' in matriz[i] or '>=' in matriz[i]:
            print("No se puede resolver por simplex, use el metodo dos fases")
        
        matriz[0].append(0.0)
        i += 1
    
    matriz[0].append(0.0)
    
    resolverSimplex(matriz)


def resolverSimplex(matriz):
    terminado = False
    iteracion = 1
    while not terminado:

        if solucionNoAcotada(matriz):
            print("Solucion no acotada")
            break

        print("Iteracion " + str(iteracion))
        imprimirMat(matriz)
        print("")

        # Busco el menor coeficiente (columna pivote)
        columna = 0.0
        for i in range(0, len(matriz[0])):
            if matriz[0][i] < columna:
                columna = matriz[0][i]

        pivotecol = matriz[0].index(columna)

        if columna >= 0:
            terminado = True
        
        else:
            # Busco el menor coeficiente (columna pivote)
            fila = float(1 << 61)

            for i in range(1, len(matriz)):
                if matriz[i][pivotecol] > 0:
                    aux = matriz[i][len(matriz[i]) - 1] / matriz[i][pivotecol]
                    if aux >= 0:
                        if fila > aux: 
                            fila = aux
                            pivotefil = i

            pivote = matriz[pivotefil][pivotecol]
            coeficientesAnt = matriz[pivotefil]
            matriz[pivotefil] = dividirFila(matriz[pivotefil], pivote)

            for i in range(0, len(matriz)):
                if i != pivotefil:
                    auxmat = matriz[i]
                    matriz[i] = restarFilas(auxmat, multiplicarFila(coeficientesAnt, matriz[i][pivotecol]))
                    
            iteracion += 1

    print("Solucion = ", str(matriz[0][len(matriz[0]) - 1]))

    return matriz


def resolverGranM(matriz, variables_salientes):
    terminado = False
    iteracion = 1
    copia_matriz = copy.deepcopy(matriz)
    coeficientes_funcion_objetivo = copia_matriz[0]
    posicion_variables_agregadas = len(matriz[0]) - len(variables_salientes)
    #variables_salientes.remove(12)
    for i in range(0, len(matriz[0])):
        # convierte los coeficientes de las variables agregadas en la funcion objetivo en tuplas (numero de la M, numero que se suma o resta)
        # desde donde empiezan las variables agregadas
        if i >= posicion_variables_agregadas:
            numero = matriz[0][i]
            matriz[0][i] = ([numero, 0.0])
        # convierte los coeficientes de la funcion objetivo en tuplas (numero de la M, numero que se suma o resta)
        # hasta donde empiezan las variables agregadas
        else:
            numero = matriz[0][i]
            matriz[0][i] = ([0.0, numero])

    for i in range(0, len(matriz[0])):
        m = 0 #contador de la lista de las variables salientes
        for j in range(1, len(matriz)):
            if i >= posicion_variables_agregadas:
                matriz[0][i][0] = matriz[0][i][0] - (matriz[j][i]*variables_salientes[m])
            else:
                matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i]*variables_salientes[m])
                m += 1 #aumenta el contador de la lista de las variables salientes

    while not terminado:
        print("Iteracion " + str(iteracion))
        if iteracion == 1:
            imprimirMatGranM(matriz,posicion_variables_agregadas)
            print("")

        if iteracion != 1:
            n = 0  #contador de la lista de los coeficientes de la funcion objetivo
            for i in range(0, len(matriz[0])):
                m = 0 #contador de la lista de las variables salientes
                for j in range(1, len(matriz)):
                    if i >= posicion_variables_agregadas:
                        if j == len(matriz)-1:
                            matriz[0][i][0] = matriz[0][i][0] - coeficientes_funcion_objetivo[n]
                            n += 1  # aumenta el contador de la lista de los coeficientes de la funcion objetivo
                        else:
                            matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i] * variables_salientes[m])
                    elif j-1 == 0:
                        if variables_salientes[m] == -1:
                            matriz[0][i][0] = (matriz[j][i] * variables_salientes[m])
                            m += 1 #aumenta el contador de la lista de las variables salientes
                        elif variables_salientes[m] != -1:
                            matriz[0][i][1] = matriz[0][i][1] + (matriz[j][i] * variables_salientes[m])
                            m += 1  # aumenta el contador de la lista de las variables salientes
                    else:
                        matriz[0][i][1] = matriz[0][i][1] + (matriz[j][i] * variables_salientes[m]) - coeficientes_funcion_objetivo[n]
                        m += 1 #aumenta el contador de la lista de las variables salientes
                        n += 1 #aumenta el contador de la lista de los coeficientes de la funcion objetivo
            imprimirMatGranM(matriz, posicion_variables_agregadas)
            print("")

        # Busco el menor coeficiente (columna pivote)
        columna = 0.0
        for i in range(0, posicion_variables_agregadas-1):
            if matriz[0][i][0] < columna:
                columna = matriz[0][i][0]
                j = i
        if columna == 0:
            for i in range(0, posicion_variables_agregadas - 1):
                if matriz[0][i][1] < columna:
                    columna = matriz[0][i][1]
                    j = i

        pivotecol = j

        if columna >= 0:
            terminado = True

        else:
            # Busco el menor coeficiente (fila pivote)
            fila = float(1 << 61)

            for i in range(1, len(matriz)):
                if matriz[i][pivotecol] > 0:
                    aux = matriz[i][posicion_variables_agregadas - 1] / matriz[i][pivotecol]
                    if aux >= 0:
                        if fila > aux:
                            fila = aux
                            pivotefil = i

            pivote = matriz[pivotefil][pivotecol]
            coeficientesAnt = matriz[pivotefil]
            matriz[pivotefil] = dividirFila(matriz[pivotefil], pivote)

            imprimirMatGranM(matriz,posicion_variables_agregadas)
            print("")

            for i in range(1, len(matriz)):
                if i != pivotefil:
                    auxmat = matriz[i]
                    matriz[i] = restarFilas(auxmat, multiplicarFila(coeficientesAnt, matriz[i][pivotecol]))

            imprimirMatGranM(matriz,posicion_variables_agregadas)
            print("")

            variables_salientes[pivotefil-1] = coeficientes_funcion_objetivo[pivotecol]
            if variables_salientes[pivotefil-1] != -1:
                for i in range(0, len(matriz[0])):
                    matriz[0][i][0] = 0.0
                    matriz[0][i][1] = 0.0

            iteracion += 1

    print("Solucion = ", str(matriz[0][posicion_variables_agregadas - 1][1]))

    return matriz

def metodoGranM(encabezado, matriz):
    i = 1
    variables_salientes = []
    matriz[0].append(0.0)
    while i < len(matriz):
        if encabezado[1] == 'max':
            if '<=' in matriz[i]:
                matriz[i].append(1.0)
                j = 1
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('<=')
                matriz[0].append(0.0)
                variables_salientes.append(0.0)

            if '=' in matriz[i]:
                matriz[i].append(1.0)
                j = 1
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('=')
                matriz[0].append(-1.0)
                variables_salientes.append(-1.0)

            if '>=' in matriz[i]:
                matriz[i].append(-1.0)
                matriz[i].append(1.0)
                j = 1
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('>=')
                matriz[0].append(0.0)
                matriz[0].append(1.0)
                variables_salientes.append(1.0)

            i += 1
        elif encabezado[1] == 'min':
            if '<=' in matriz[i]:
                matriz[i].append(1.0)
                j = 1
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('<=')
                matriz[0].append(0.0)
                variables_salientes.append(12)
                variables_salientes.append(0.0)

            if '=' in matriz[i]:
                matriz[i].append(1.0)
                j = 1
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('=')
                matriz[0].append(1.0)
                variables_salientes.append(1.0)

            if '>=' in matriz[i]:
                matriz[i].append(-1.0)
                matriz[i].append(1.0)
                j = 1
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('>=')
                matriz[0].append(0.0)
                matriz[0].append(1.0)
                variables_salientes.append(12)
                variables_salientes.append(1.0)

            i += 1

    resolverGranM(matriz,variables_salientes)
    pass


def quitarColumnas(matriz):
    nmatriz = []
    for i in range(0, len(matriz)):
        tmp = []
        for j in range(0, len(matriz[i])):
            if matriz[0][j] != 1:
                tmp.append(matriz[i][j])

        nmatriz.append(tmp)

    return nmatriz


def metodoDosFases(encabezado, matriz):
    i = 1
    while i < len(matriz):
        if '<=' in matriz[i]:
            indice = matriz[i].index('<=')
            matriz[i].insert(indice,1) 
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0) 
                j += 1

            matriz[0].append(0.0)
            matriz[i].remove('<=')

        if '=' in matriz[i]:
            indice = matriz[i].index('=')
            matriz[i].insert(indice,1) 
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0) 
                j += 1

            matriz[0].append(0.0)
            matriz[i].remove('=')

        if '>=' in matriz[i]:
            indice = matriz[i].index('>=')
            matriz[i].insert(indice,-1.0)
            matriz[i].insert(indice + 1,1.0)
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0.0) 
                    matriz[j].insert(indice + 1,0.0) 
                j += 1
            
            matriz[0].append(0.0)
            matriz[0].append(-1.0)

            matriz[i].remove('>=')
        
        i += 1
    
    matriz[0].append(0.0)
    
    mmatriz = copiarMatriz(matriz)

    limiteOrginal = int(encabezado[2])

    for i in range(0, limiteOrginal):
        mmatriz[0][i] = 0

    for j in range(limiteOrginal, len(mmatriz[0])):
        unoEncontrado = False
        menosUnoEncontrado = False
        for i in range(0, len(mmatriz)):
            if mmatriz[i][j] == 1:
                unoEncontrado = True

            elif mmatriz[i][j] == -1:
                menosUnoEncontrado = True
            
            if unoEncontrado and menosUnoEncontrado:
                mmatriz[0] = sumarFilas(mmatriz[0], mmatriz[i])
    
    
    for i in range(0, len(mmatriz[0])):
        mmatriz[0][i] = mmatriz[0][i] * -1


    # Fin primera fase
    mmatriz = resolverSimplex(mmatriz)

    nmatriz = quitarColumnas(mmatriz)

    for i in range(0, int(encabezado[2])):
        nmatriz[0][i] = matriz[0][i] * -1

    # Solucion segunda fase
    pmatriz = resolverSimplex(nmatriz)


def metodoDual(encabezado, matriz):
    pass


def resolverProblema(ruta):
    '''
    formato del archivo:

    metodo,operacion,variables,restriciones
    ecuacion objetivo
    restriciones
    '''

    try:

        # Leo el archivo
        descriptor = open(ruta, mode='r', encoding='utf-8')
        contenido = descriptor.read()
        descriptor.close()

        # Obtengo las lineas del archivo
        lineas = contenido.split('\n')

        # Separo el encabezado del ejercicio
        encabezado = lineas[0].split(',')
        
        # Separo las demás lineas del archivo
        matriz = []
        i = 1

        while i < int(encabezado[3]) + 2:
            matriz += [lineas[i].split(',')]
            i += 1

        # Convierto a numeros
        i = 0
        while i < len(matriz):
            j = 0
            while j < len(matriz[i]):
                if matriz[i][j] != '<=' and matriz[i][j] != '>=' and matriz[i][j] != '=':
                    matriz[i][j] = float(matriz[i][j])
                
                j += 1
            i += 1

        # Solo para prueba de parseo
        # print(matriz)

        if int(encabezado[0]) == 0:
            metodoSimplex(encabezado, matriz)

        elif int(encabezado[0]) == 1:
            metodoGranM(encabezado, matriz)

        elif int(encabezado[0]) == 2:
            metodoDosFases(encabezado, matriz)

        elif int(encabezado[0]) == 3:
            metodoDual(encabezado, matriz)

    except IOError:
        print("El archivo " + ruta + " no existe")


def main():
    numeroDeArgumentos = len(sys.argv)

    if numeroDeArgumentos < 2:
        print("Para más informacion ejecute:\n")
        print("python simplex.py -h\n")

    # Ejecuto la operacion de cada archivo en los argumentos
    elif numeroDeArgumentos >= 2:
        problema = 1

        while problema < numeroDeArgumentos:
            if sys.argv[problema] == '-h':
                print("Ejemplo de ejecucion del programa:\n")
                print("python simplex.py [-h] archivo.txt [archivo2.txt archivo3.txt ...]\n")

            else:
                resolverProblema(sys.argv[problema])
        
            problema += 1


if __name__ == "__main__":
    main()