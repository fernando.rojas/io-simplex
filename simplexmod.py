import sys
import copy

def imprimirMat(matriz):
    print()
    for i in range(0, len(matriz)):
        for j in range(0, len(matriz[i])):
            print("%8.2f" % matriz[i][j], end=" ")
        print("")
    
    print("")

def escribir(ruta,string):
    archivo = open(ruta,'a')
    archivo.write(string)
    archivo.write("\n\n")
    archivo.close()

def escribir_numero_iteracion(ruta,string):
    archivo = open(ruta,'a')
    archivo.write("\n\n")
    archivo.write(string)
    archivo.write("\n\n")
    archivo.close()

def escribir_valores_iteracion(ruta,string):
    archivo = open(ruta,'a')
    archivo.write(string)
    archivo.write("    ")
    archivo.close()

def parsear_matriz(matriz,f0,c0):
    resultado = ""
    for elem in f0:
        resultado+= elem + "      "
    resultado += "\n"
    for i in range(0, len(matriz)):
        resultado+= c0[i]+" "
        for j in range(0, len(matriz[i])):
            temp_coeficiente = str(matriz[i][j])
            if(7 < len(temp_coeficiente)):
                resultado += temp_coeficiente[0:7]+" "
            else:
                for x in range(0,7-len(temp_coeficiente)):
                    resultado+=" "
                resultado+=temp_coeficiente+" "
        resultado+="\n"
    return resultado


def parsear_matriz_gran_M(matriz,f0,c0, posicion_variables_agregadas):
    resultado = ""
    for elem in f0:
        resultado+= elem + "         "
    resultado += "\n" 
    for i in range(0, len(matriz)):
        resultado+= c0[i]+"     "
        for j in range(0, len(matriz[i])):
            if i == 0:
                if j < posicion_variables_agregadas-1:
                    if matriz[i][j][0] == 0.0:
                        numero = round(matriz[i][j][1], 3)
                        resultado += "   " + str(numero) + "     "
                    elif matriz[i][j][1] == 0.0:
                        numero = round(matriz[i][j][0], 2)
                        resultado += str(numero) + 'M' + "    "
                    elif matriz[i][j][1] > 0:
                        numero = round(matriz[i][j][0], 2)
                        numero1 = round(matriz[i][j][1], 3)
                        resultado += str(numero) + 'M-' + str(numero1) + "  "
                    else:
                        numero = round(matriz[i][j][0], 2)
                        numero1 = round(matriz[i][j][1], 3)
                        resultado += str(numero) + 'M' + str(numero1) + "  "
                elif j >= posicion_variables_agregadas:
                    if matriz[i][j][0] == 0.0:
                        numero = round(matriz[i][j][1], 3)
                        resultado += "  " + str(numero) + "    "
                    elif matriz[i][j][1] == 0.0:
                        numero = round(matriz[i][j][0], 2)
                        resultado += str(numero) + 'M' + "       "
                    elif matriz[i][j][1] > 0:
                        numero = round(matriz[i][j][0], 2)
                        numero1 = round(matriz[i][j][1], 3)
                        resultado += str(numero) + 'M-' + str(numero1) + "  "
                    else:
                        numero = round(matriz[i][j][0], 2)
                        numero1 = round(matriz[i][j][1], 3)
                        resultado += str(numero) + 'M' + str(numero1) + "  "
                else:
                    m = j
            elif j < posicion_variables_agregadas-1:
                numero = round(matriz[i][j], 3)
                resultado += " " +str(numero) + "        "
            elif j >= posicion_variables_agregadas:
                numero = round(matriz[i][j], 3)
                resultado += str(numero) + "      "
            else:
                m = j
        # Aqui se imprime el lado derecho
        if i == 0:
            if matriz[i][m][0] == 0.0:
                numero = round(matriz[i][m][1], 3)
                resultado += str(numero)
            elif matriz[i][m][1] == 0.0:
                numero = round(matriz[i][m][0], 2)
                resultado += str(numero) + 'M'
            elif matriz[i][m][1] > 0:
                numero = round(matriz[i][m][0], 2)
                numero1 = round(matriz[i][m][1], 3)
                resultado += str(numero) + 'M+' + str(numero1)
            else:
                numero = round(matriz[i][m][0], 2)
                numero1 = round(matriz[i][m][1], 3)
                resultado += str(numero) + 'M' + str(numero1)
        else:
            numero = round(matriz[i][m], 3)
            resultado += str(numero)
        resultado+="\n"
    return resultado

def dividirFila(fila, escalar):
    '''
    divide los valores de una fila entre un escalar
    '''
    for i in range(0,len(fila)):
        fila[i] = fila[i]/escalar
    
    return fila


def multiplicarFila(fila, escalar):
    '''
    multiplica los valores de una fila entre un escalar
    '''
    nfila = []
    for i in range(0,len(fila)):
        nfila.append(fila[i]*escalar)
    
    return nfila


def restarFilas(fila, fila2):
    '''
    resta dos filas
    '''
    nfila = []
    for i in range(0,len(fila)):
        nfila.append(fila[i]-fila2[i])
    
    return nfila


def sumarFilas(fila, fila2):
    '''
    suma dos filas
    '''
    nfila = []
    for i in range(0,len(fila)):
        nfila.append(fila[i]+fila2[i])
    
    return nfila


def copiarMatriz(matriz):
    '''
    copia una matriz
    '''
    nmatriz = []
    for i in range(0,len(matriz)):
        nfila = []
        for j in range(0,len(matriz[i])):
            nfila.append(matriz[i][j])
        nmatriz.append(nfila)

    return nmatriz



def estetica(variables, restriciones):
    wea = 1
    fila_master = ["VB"]
    while(wea<variables+1):
        fila_master.append("X"+str(wea))
        wea+=1
    columna_master = ["U "]
    while (wea < restriciones+variables+1):
        columna_master.append("X"+str(wea))
        wea+=1
    fila_master += columna_master[1:]    
    fila_master.append("LD")
    return [fila_master, columna_master]


def metodoSimplex(encabezado, matriz,arch):

    #Igualar a 0 la funcion objetivo
    if 'max' in encabezado:
        matriz[0] = multiplicarFila(matriz[0], -1)

    i = 1
    while i < len(matriz):
        if '<=' in matriz[i]:
            indice = matriz[i].index('<=')
            matriz[i].insert(indice,1.0) 
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0.0) 
                j += 1

            matriz[i].remove('<=')

        if '=' in matriz[i] or '>=' in matriz[i]:
            print("No se puede resolver por simplex, use el metodo dos fases")
        
        matriz[0].append(0.0)
        i += 1
    
    matriz[0].append(0.0)
    
    resolverSimplex(matriz,arch,int(encabezado[2]),int(encabezado[3]))
    pass



def parsear_matriz2(matriz):
    resultado = ""
    for i in range(0, len(matriz)):
        for j in range(0, len(matriz[i])):
            temp_coeficiente = str(matriz[i][j])
            if(7 < len(temp_coeficiente)):
                resultado += temp_coeficiente[0:7]+" "
            else:
                for x in range(0,7-len(temp_coeficiente)):
                    resultado+=" "
                resultado+=temp_coeficiente+" "
        resultado+="\n"
    return resultado


def solucionNoAcotada(matriz):
    for i in range(0, len(matriz[0])):
        columnaNoValida = True
        for j in range(0, len(matriz)):
            if matriz[j][i] > 0:
                columnaNoValida = False

        if columnaNoValida:
            return columnaNoValida

    return columnaNoValida

def solucionNoAcotadaGranM(matriz):
    for i in range(0, len(matriz[0])):
        columnaNoValida = True
        for j in range(0, len(matriz)):
            if j == 0:
                if matriz[0][i][0] >= 0 or matriz[0][i][1] >= 0:
                    columnaNoValida = False
            else:
                if matriz[j][i] >= 0:
                    columnaNoValida = False

        if columnaNoValida:
            return columnaNoValida

    return columnaNoValida

def resolverSimplex(matriz,arch,variables=None,restriciones=None):
    if(variables!=None):
        fc = estetica(variables,restriciones)
        f0 = fc[0]
        c0 = fc[1] 
    terminado = False
    iteracion = 1
    while not terminado:

        if solucionNoAcotada(matriz):
            print("Solucion no acotada")
            escribir(arch, "Solucion no acotada")
            return None
            break

        escribir(arch,"Iteracion " + str(iteracion))
        if(variables!=None):
            escribir(arch,parsear_matriz(matriz,f0,c0))
        else:
            escribir(arch,parsear_matriz2(matriz))

        #print("")
        columna = 0.0
        for i in range(0, len(matriz[0])):
            if matriz[0][i] < columna:
                columna = matriz[0][i]

        pivotecol = matriz[0].index(columna)

        if columna >= 0:
            terminado = True
        
        else:
            # Busco el menor coeficiente (columna pivote)
            fila = float(1 << 61)

            for i in range(1, len(matriz)):
                if matriz[i][pivotecol] > 0:
                    aux = matriz[i][len(matriz[i]) - 1] / matriz[i][pivotecol]
                    if aux >= 0:
                        if fila > aux: 
                            fila = aux
                            pivotefil = i

            pivote = matriz[pivotefil][pivotecol] #pivote
            escribir(arch,"Pivote-Iteracion: " + str(pivote))
            
            if(variables!=None):
                c0[pivotefil] = f0[pivotecol+1]


            coeficientesAnt = matriz[pivotefil]
            matriz[pivotefil] = dividirFila(matriz[pivotefil], pivote)

            #imprimirMat(matriz)
            #print("")

            for i in range(0, len(matriz)):
                if i != pivotefil:
                    auxmat = matriz[i]
                    matriz[i] = restarFilas(auxmat, multiplicarFila(coeficientesAnt, matriz[i][pivotecol]))
                    
            #imprimirMat(matriz)
            #print("")

            iteracion += 1

    escribir(arch,"Solucion = " + str(matriz[0][len(matriz[0]) - 1]))

    return matriz

"""
 Funcion que resuelve problemas del metodo de la gran M
 Parametros:
 matriz: coeficientes de la funcion objetivo y de las restricciones 
 variables_salientes: coeficientes de la variables a salir 
 arch: nombre del archivo solucion
 variables=None: Número de variables de decisión
"""

def resolverGranM(matriz, variables_salientes,arch, variables=None, optimizacion=None):
    # Calcula el numero de letras dependiente del número de variables de decisión. Ej: X1,X2,X3...
    if(variables!=None):
        fc = estetica(variables,len(variables_salientes))
        f0 = fc[0] #Aqui se guardan las variables no basicas
        c0 = fc[1] #Aqui se guardan las variables no basicas
    terminado = False #Condicion para terminar las iteraciones
    iteracion = 1 #Numero de iteracion
    copia_matriz = copy.deepcopy(matriz) #Copia de los coeficientes de la matriz inicial
    coeficientes_funcion_objetivo = copia_matriz[0] #Copia de los coeficientes de la funcion objetivo del problema
    posicion_variables_agregadas = len(matriz[0]) - len(variables_salientes) #Posicion desde que empiezan las variables agregadas

    #El capturador de errores borra el coeficiente de la variable de holgura de las variables agregadas
    try:
        for i in range(0, len(variables_salientes)):
            posicion_variable_holgura = variables_salientes.index(10)
            del c0[posicion_variable_holgura + 1]
            variables_salientes.remove(10)
    except:
        pass

    # Este ciclo convierte los coeficientes de la funcion objetivo en tuplas
    for i in range(0, len(matriz[0])): # Aqui se recorre cada columna de la funcion objetivo
        # Aqui se convierten los coeficientes de las variables agregadas (numero de la M, numero que se suma o resta) Ej: 10M + 5 -> [10,0]
        if i >= posicion_variables_agregadas: # desde donde empiezan las variables agregadas
            numero = matriz[0][i]
            matriz[0][i] = ([numero, 0.0])
        # convierte los coeficientes de la funcion objetivo en tuplas (numero de la M, numero que se suma o resta) Ej: 10M + 5 -> [0,5]
        else: # hasta donde empiezan las variables agregadas
            numero = matriz[0][i]
            matriz[0][i] = ([0.0, -numero])

    """ - Esto ciclo es para obtener los valores de la funcion objetivo con Z_j - C_j
        recorriendo la matriz con los coeficientes de la función objetivo del problema (C_j) y de las restricciones (X_Y)
        - Siguiendo la formula Z_j = Σ C_B * X_Y
        - Donde C_B son los coeficientes de la variables a salir, y X_Y son los coeficientes de las restricciones
    """
    for i in range(0, len(matriz[0])): #Aqui se recorre las columnas de la matriz
        m = 0 #contador de la lista de las variables a salir
        for j in range(1, len(matriz)): #Aqui se recorren las filas de la matriz
            if i >= posicion_variables_agregadas: # desde donde empiezan las variables agregadas
                if m == len(variables_salientes)-1: # si llegue a la ultima columna de las variables agregadas
                    matriz[0][i][0] = (matriz[j][i]*variables_salientes[m]) + matriz[0][i][0]
                    matriz[0][i][0] -= coeficientes_funcion_objetivo[i]
                    m += 1
                elif m == 0: # si es la primera columna de las variables agregadas
                    matriz[0][i][0] = (matriz[j][i]*variables_salientes[m])
                    m += 1
                else: # si es cualquier columna que no sea ni la primera ni la ultima de las variables agregadas
                    matriz[0][i][0] = (matriz[j][i] * variables_salientes[m]) + matriz[0][i][0]
                    m += 1
            else: # hasta donde empiezan las variables agregadas
                matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i]*variables_salientes[m])
                m += 1 #aumenta el contador de la lista de las variables salientes

    # Mientras haya un coeficiente negativo en la funcion objetivo, siga iterando
    while not terminado:
        if solucionNoAcotadaGranM(matriz):
            print("Solucion no acotada")
            escribir(arch, "Solucion no acotada")
            return None
            break
        # Aqui imprimo la tabla inicial
        if iteracion == 1:
            if (variables != None):
                escribir(arch, "Estado " + str(iteracion)) #Guardo en texto al archivo solucion
                escribir(arch, parsear_matriz_gran_M(matriz, f0, c0, posicion_variables_agregadas)) #Guardo en texto al archivo solucion

        # Busco el menor coeficiente (columna pivote)
        columna = 0.0
        if optimizacion == 'max':
            for i in range(0, posicion_variables_agregadas-1):
                if matriz[0][i][0] < columna:
                    columna = matriz[0][i][0]
                    j = i
        if columna == 0:
            for i in range(0, posicion_variables_agregadas - 1):
                if matriz[0][i][1] < columna:
                    columna = matriz[0][i][1]
                    j = i

        pivotecol = j #Aqui guardo la posicion del coeficiente de la columna pivote

        # Esta es la condicion de parada del ciclo, cuando ya no hay coeficientes negativos en la funcion objetivo
        if columna >= 0:
            terminado = True

        else:
            # Busco el menor coeficiente (fila pivote)
            fila = float(1 << 61)

            for i in range(1, len(matriz)):
                if matriz[i][pivotecol] > 0:
                    aux = matriz[i][posicion_variables_agregadas - 1] / matriz[i][pivotecol] #Divido el coeficiente del lado derecho entre los coeficientes de la columna pivote
                    if aux >= 0: #Eligo el numero menor
                        if fila > aux:
                            fila = aux
                            pivotefil = i

            pivote = matriz[pivotefil][pivotecol] #Aqui el coeficiente pivote
            escribir_valores_iteracion(arch, "Número Pivot: " + str(pivote)) #Guardo en texto al archivo solucion
            escribir_valores_iteracion(arch, "VB entrante: " + fc[0][pivotecol+1]) #Guardo en texto al archivo solucion
            escribir_valores_iteracion(arch, "VB saliente: " + fc[1][pivotefil]) #Guardo en texto al archivo solucion
            iteracion += 1

            # Aqui se da el cambio de la letra de la variable entrante por la letra de la variable saliente
            if (variables != None):
                c0[pivotefil] = f0[pivotecol + 1]
            # Guardo los coeficientes de la fila del numero pivote
            coeficientesAnt = matriz[pivotefil]
            # Divido el numero pivote entre toda su fila
            matriz[pivotefil] = dividirFila(matriz[pivotefil], pivote)

            # Este ciclo hace las operaciones la nueva fila entre la tabla anterior para obtener la nueva tabla
            for i in range(1, len(matriz)):
                if i != pivotefil:
                    auxmat = matriz[i]
                    matriz[i] = restarFilas(auxmat, multiplicarFila(coeficientesAnt, matriz[i][pivotecol]))

            variables_salientes[pivotefil-1] = coeficientes_funcion_objetivo[pivotecol] #Aqui se da el cambio del coeficiente de la variable entrante por el coeficiente de la variable saliente
            # Pongo el cero la funcion objetivo para poder volver a calcularla
            if variables_salientes[pivotefil-1] != -1:
                for i in range(0, len(matriz[0])):
                    matriz[0][i][0] = 0.0
                    matriz[0][i][1] = 0.0

            """ - Esto ciclo es para obtener los valores de la funcion objetivo con Z_j - C_j
                recorriendo la matriz con los coeficientes de la función objetivo del problema (C_j) y de las restricciones (X_Y)
                - Siguiendo la formula Z_j = Σ C_B * X_Y
                - Donde C_B son los coeficientes de la variables a salir, y X_Y son los coeficientes de las restricciones
            """
            n = 0  # contador de la lista de los coeficientes de la funcion objetivo
            for i in range(0, len(matriz[0])):
                m = 0  # contador de la lista de las variables salientes
                for j in range(1, len(matriz)):
                    if i >= posicion_variables_agregadas:
                        if j == len(matriz) - 1:
                            if variables_salientes[m] != -1 and variables_salientes[m] != 1:
                                matriz[0][i][1] = matriz[0][i][1] + (matriz[j][i] * variables_salientes[m])
                                m += 1  # aumenta el contador de la lista de las variables salientes
                            elif variables_salientes[m-1] != -1 and n == 4 and matriz[0][i][0] == -1:
                                n += 1
                            elif coeficientes_funcion_objetivo[n] == -1:
                                matriz[0][i][0] = matriz[0][i][0] - coeficientes_funcion_objetivo[n]
                                n += 1  # aumenta el contador de la lista de los coeficientes de la funcion objetivo
                            elif coeficientes_funcion_objetivo[n] == 0 or coeficientes_funcion_objetivo[n] == 1:
                                if coeficientes_funcion_objetivo[n] == 1:
                                    matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i] * variables_salientes[m]) - coeficientes_funcion_objetivo[n]
                                else:
                                    matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i] * variables_salientes[m])
                        elif j != len(matriz) - 1:
                            if m == 0:
                                matriz[0][i][1] = matriz[0][i][1] + (matriz[j][i] * variables_salientes[m])
                                m += 1  # aumenta el contador de la lista de las variables salientes
                        else:
                            matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i] * variables_salientes[m])
                            m += 1  # aumenta el contador de la lista de las variables salientes
                    elif j - 1 == 0:
                        if variables_salientes[m] == -1:
                            matriz[0][i][0] = (matriz[j][i] * variables_salientes[m])
                            m += 1  # aumenta el contador de la lista de las variables salientes
                        elif variables_salientes[m] != -1:
                            if j == len(matriz)-1:
                                matriz[0][i][1] = matriz[0][i][1] + (matriz[j][i] * variables_salientes[m]) - coeficientes_funcion_objetivo[n]
                                m += 1  # aumenta el contador de la lista de las variables salientes
                            elif j != len(matriz)-1:
                                if variables_salientes[m+1] == 1:
                                    matriz[0][i][1] = matriz[0][i][1] + (matriz[j][i] * variables_salientes[m]) - coeficientes_funcion_objetivo[n]
                                    m += 1  # aumenta el contador de la lista de las variables salientes
                                else:
                                    matriz[0][i][1] = matriz[0][i][1] + (matriz[j][i] * variables_salientes[m])
                                    m += 1  # aumenta el contador de la lista de las variables salientes
                    elif m != 0 and m != len(variables_salientes)-1:
                        if variables_salientes[m] == -1:
                            matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i] * variables_salientes[m])
                            m += 1  # aumenta el contador de la lista de las variables salientes
                    elif m >= len(variables_salientes)-1 and variables_salientes[m] == 1:
                        matriz[0][i][0] = matriz[0][i][0] + (matriz[j][i] * variables_salientes[m])
                        m += 1  # aumenta el contador de la lista de las variables salientes
                    else:
                        matriz[0][i][1] = matriz[0][i][1] + matriz[j][i] * variables_salientes[m]
                        matriz[0][i][1] = round(matriz[0][i][1]) - coeficientes_funcion_objetivo[n]
                        m += 1  # aumenta el contador de la lista de las variables salientes
                n += 1
            # Aqui guardo en archivo solucion las tablas intermedias
            if (variables != None):
                escribir_numero_iteracion(arch, "Estado " + str(iteracion)) #Guardo en texto al archivo solucion
                escribir(arch, parsear_matriz_gran_M(matriz, f0, c0, posicion_variables_agregadas)) #Guardo en texto al archivo solucion

    # Imprimo en consola la solucion del problema
    texto = "("
    for i in range(1, len(matriz)):
        if i == len(matriz)-1:
            texto += str(matriz[i][posicion_variables_agregadas - 1])
        else:
            texto += str(matriz[i][posicion_variables_agregadas - 1]) + ","
    texto += ")"
    escribir(arch, "Respuesta Final: U = " + str(matriz[0][posicion_variables_agregadas - 1][1]))
    print("Respuesta Final: U = " + str(matriz[0][posicion_variables_agregadas - 1][1]) + ", " + texto)

    return matriz

# Esta funcion genera la matriz de solo coeficientes a partir de las restricciones
# Agrega coeficientes dependiendo del signo de la restriccion
def metodoGranM(encabezado, matriz,arch):
    i = 1
    variables_salientes = []
    matriz[0].append(0.0)
    while i < len(matriz):
        #Si es un problema de maximizacion
        if encabezado[1] == 'max':
            if '<=' in matriz[i]: # Agrega los coeficientes respectivo si el signo es un "<="
                matriz[i].append(1.0)
                j = 1
                # Agrego ceros a las demas columnas para rellenar la matriz
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('<=')
                matriz[0].append(0.0)
                variables_salientes.append(0.0)

            if '=' in matriz[i]: # Agrega los coeficientes respectivo si el signo es un "="
                matriz[i].append(1.0)
                j = 1
                # Agrego ceros a las demas columnas para rellenar la matriz
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('=')
                matriz[0].append(-1.0)
                variables_salientes.append(-1.0)

            if '>=' in matriz[i]: # Agrega los coeficientes respectivo si el signo es un ">="
                matriz[i].append(-1.0)
                matriz[i].append(1.0)
                j = 1
                # Agrego ceros a las demas columnas para rellenar la matriz
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('>=')
                matriz[0].append(0.0)
                matriz[0].append(-1.0)
                variables_salientes.append(10)
                variables_salientes.append(-1.0)

            i += 1 # Contador para recorrer las restricciones

        # Si es un problema de minimizacion
        elif encabezado[1] == 'min':
            if '<=' in matriz[i]: # Agrega los coeficientes respectivo si el signo es un "<="
                matriz[i].append(1.0)
                j = 1
                # Agrego ceros a las demas columnas para rellenar la matriz
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('<=')
                matriz[0].append(0.0)
                variables_salientes.append(0.0)

            if '=' in matriz[i]: # Agrega los coeficientes respectivo si el signo es un "="
                matriz[i].append(1.0)
                j = 1
                # Agrego ceros a las demas columnas para rellenar la matriz
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('=')
                matriz[0].append(1.0)
                variables_salientes.append(1.0)

            if '>=' in matriz[i]: # Agrega los coeficientes respectivo si el signo es un ">="
                matriz[i].append(-1.0)
                matriz[i].append(1.0)
                j = 1
                # Agrego ceros a las demas columnas para rellenar la matriz
                while j < len(matriz):
                    if i != j:
                        matriz[j].append(0.0)
                        matriz[j].append(0.0)
                    j += 1

                matriz[i].remove('>=')
                matriz[0].append(0.0)
                matriz[0].append(1.0)
                variables_salientes.append(10)
                variables_salientes.append(1.0)

            i += 1 # Contador para recorrer las restricciones

    # Una vez la matriz tenga solo coeficientes numericos, se llama a la funcion que resuelve el problema con el metodo de gran M
    resolverGranM(matriz,variables_salientes,arch, int(encabezado[2]), encabezado[1])
    pass


def quitarColumnas(matriz):
    nmatriz = []
    for i in range(0, len(matriz)):
        tmp = []
        for j in range(0, len(matriz[i])):
            if matriz[0][j] != 1:
                tmp.append(matriz[i][j])

        nmatriz.append(tmp)

    return nmatriz


def metodoDosFases(encabezado, matriz, arch):
    i = 1
    while i < len(matriz):
        if '<=' in matriz[i]:
            indice = matriz[i].index('<=')
            matriz[i].insert(indice,1) 
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0) 
                j += 1

            matriz[0].append(0.0)
            matriz[i].remove('<=')

        if '=' in matriz[i]:
            indice = matriz[i].index('=')
            matriz[i].insert(indice,1) 
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0) 
                j += 1

            matriz[0].append(0.0)
            matriz[i].remove('=')

        if '>=' in matriz[i]:
            indice = matriz[i].index('>=')
            matriz[i].insert(indice,-1.0)
            matriz[i].insert(indice + 1,1.0)
            j = 1
            while j < len(matriz):
                if i != j:
                    matriz[j].insert(indice,0.0) 
                    matriz[j].insert(indice + 1,0.0) 
                j += 1
            
            matriz[0].append(0.0)
            matriz[0].append(-1.0)

            matriz[i].remove('>=')
        
        i += 1
    
    matriz[0].append(0.0)
    
    mmatriz = copiarMatriz(matriz)

    limiteOrginal = int(encabezado[2])

    for i in range(0, limiteOrginal):
        mmatriz[0][i] = 0

    for j in range(limiteOrginal, len(mmatriz[0])):
        unoEncontrado = False
        menosUnoEncontrado = False
        for i in range(0, len(mmatriz)):
            if mmatriz[i][j] == 1:
                unoEncontrado = True

            elif mmatriz[i][j] == -1:
                menosUnoEncontrado = True
            
            if unoEncontrado and menosUnoEncontrado:
                mmatriz[0] = sumarFilas(mmatriz[0], mmatriz[i])
    
    
    for i in range(0, len(mmatriz[0])):
        mmatriz[0][i] = mmatriz[0][i] * -1

    escribir(arch,"Primera Fase")
    # Fin primera fase
    mmatriz = resolverSimplex(mmatriz,arch)
    escribir(arch, "Segunda Fase")

    nmatriz = quitarColumnas(mmatriz)

    for i in range(0, int(encabezado[2])):
        nmatriz[0][i] = matriz[0][i] * -1

    # Solucion segunda fase
    pmatriz = resolverSimplex(nmatriz,arch)


def metodoDual(encabezado, matriz):
    pass


def resolverProblema(ruta):
    '''
    formato del archivo:

    metodo,operacion,variables,restriciones
    ecuacion objetivo
    restriciones
    '''

    try:

        # Leo el archivo
        descriptor = open(ruta, mode='r', encoding='utf-8')
        contenido = descriptor.read()
        descriptor.close()

        # Obtengo las lineas del archivo
        lineas = contenido.split('\n')

        # Separo el encabezado del ejercicio
        encabezado = lineas[0].split(',')
        
        # Separo las demás lineas del archivo
        matriz = []
        i = 1

        while i < int(encabezado[3]) + 2:
            matriz += [lineas[i].split(',')]
            i += 1

        # Convierto a numeros
        i = 0
        while i < len(matriz):
            j = 0
            while j < len(matriz[i]):
                if matriz[i][j] != '<=' and matriz[i][j] != '>=' and matriz[i][j] != '=':
                    matriz[i][j] = float(matriz[i][j])
                
                j += 1
            i += 1

        # Solo para prueba de parseo
        # print(matriz)
        print(encabezado)

        if int(encabezado[0]) == 0:
            metodoSimplex(encabezado, matriz)

        elif int(encabezado[0]) == 1:
            metodoGranM(encabezado, matriz)

        elif int(encabezado[0]) == 2:
            metodoDosFases(encabezado, matriz)

        elif int(encabezado[0]) == 3:
            metodoDual(encabezado, matriz)

    except IOError:
        print("El archivo " + ruta + " no existe")


def main():
    numeroDeArgumentos = len(sys.argv)

    if numeroDeArgumentos < 2:
        print("Para más informacion ejecute:\n")
        print("python simplex.py -h\n")

    # Ejecuto la operacion de cada archivo en los argumentos
    elif numeroDeArgumentos >= 2:
        problema = 1

        while problema < numeroDeArgumentos:
            if sys.argv[problema] == '-h':
                print("Ejemplo de ejecucion del programa:\n")
                print("python simplex.py [-h] archivo.txt [archivo2.txt archivo3.txt ...]\n")

            else:
                resolverProblema(sys.argv[problema])
        
            problema += 1


if __name__ == "__main__":
    main()