import argparse
from simplexmod import *
import dual as dual






class Problema:
    def __init__(self):
        self.metodo = 0
        self.tipo_optimizacion = ''
        self.num_var_decision = 0
        self.num_restricciones = 0
        self.variables = []
        self.matriz = []
        self.nombre_arch_salida = ""

    def matriz_inicial_toString(self):
        salida = ''
        for i in self.matriz:
            if(len(i) == 0):
                break
            salida += i[0]+'X'+str(1)
            for j in range(1, self.num_var_decision):
                salida += ' + ' + i[j]+'X'+str(j+1)
            salida+= ' ' + i[-2] + ' ' + i[-1]
            salida+= '\n'
        return salida
    
    def metodo_string(self):
    	if(self.metodo == 0):
    		return "Simplex"
    	elif(self.metodo == 1):
    		return "GrandM"
    	elif(self.metodo == 2):
    		return "2 Fases"
    	elif(self.metodo == 3):
    		return "Dual"     


    def dar_parametros_dual(self):
    	p1= str(self.variables[0])
    	for i in range(1,self.num_var_decision):
    		p1+= ','+str(self.variables[i])
    	p2=[]
    	
    	for h in self.matriz:
    		p2.append(','.join(h))
    	
    	return [p1, p2]

    def dar_parametros_granM(self):
        p1 = [self.metodo, self.tipo_optimizacion, self.num_var_decision, self.num_restricciones]
        p2 = []
        variables = []
        for y in self.variables:
            variables.append(float(y))
        p2.append(variables)


        for i in self.matriz:
            temp_fila = []
            for j in range(0,self.num_var_decision):
                temp_fila.append(float(i[j]))
            temp_fila.append(i[self.num_var_decision])
            temp_fila.append(float(i[self.num_var_decision+1]))
            p2.append(temp_fila)


        return [p1,p2]


    def dar_parametros_Simplex(self):
        p1 = [self.metodo, self.tipo_optimizacion, self.num_var_decision, self.num_restricciones]
        p2 = []
        variables = []
        for y in self.variables:
            variables.append(float(y))
        p2.append(variables)


        for i in self.matriz:
            temp_fila = []
            for j in range(0,self.num_var_decision):
                temp_fila.append(float(i[j]))
            temp_fila.append(i[self.num_var_decision])
            temp_fila.append(float(i[self.num_var_decision+1]))
            p2.append(temp_fila)


        return [p1,p2]





        
        

    def funcion_U_toString(self):
        salida = str(self.variables[0])+'X'+str(1)        
        for i in range(1,self.num_var_decision):
            salida+= ' + ' + str(self.variables[i])+'X'+str(i+1)
        salida += ' = '+self.tipo_optimizacion
        return salida

    def toString(self):
        salida = ""
        salida+= "Metodo: "+ self.metodo_string() + "\n"
        salida+= "Optimizacion: "+ self.tipo_optimizacion + "\n"
        salida+= "Variables de decision: "+ str(self.num_var_decision) + "\n"
        salida+= "Restricciones: "+ str(self.num_restricciones) + "\n"
        salida+= "Funcion U: "+ self.funcion_U_toString() + "\n"
        salida+= "Restrucciones: \n"+ self.matriz_inicial_toString() + "\n"
        return salida
        


def cargar_problema(problema, entrada):
    f = open(entrada,'r')
    texto = f.read() 
    f.close()
    texto_lista = texto.split('\n')
    linea1 = texto_lista[0].split(',')    
    problema.metodo = int(linea1[0])
    problema.tipo_optimizacion = linea1[1]
    problema.num_var_decision = int(linea1[2])
    problema.num_restricciones = int(linea1[3])
    linea2 = texto_lista[1].split(',')
    for i in linea2:
        problema.variables.append(int(i))
    for i in range(2,problema.num_restricciones+2):
        problema.matriz.append(texto_lista[i].split(','))
    



'''


def estetica(encabezado):
    wea = 1
    fila_master = ["VM"]
    while(wea<int(encabezado[2])+1):
        fila_master.append("X"+str(wea))
        wea+=1
    columna_master = ["U "]
    for weafea in range(1,int(encabezado[3])+1):
        columna_master.append("X"+str(wea))
        wea+=1
    fila_master += columna_master[1:]    
    fila_master.append("LD")
    return [fila_master, columna_master]

'''
def yuberta(matriz):
	r = ""
	for i in matriz:
		for j in i:
			if(isinstance(j,str)):
				r += j
			else:
				r+= str(j)
		r+='\n'
	return r





parser = argparse.ArgumentParser(description='Metodo Simplex')
parser.add_argument(dest = 'problemas', 
                    metavar = 'problemas', nargs = '*', help= "Ruta de 1 o mas problemas separados por texto")
args = parser.parse_args()
entradas = args.problemas


num = 0

if(len(entradas) == 0):
	pass
	print("Requiere al menos un problema para funcionar, mas info -h")
else:
    for i in entradas:
        optimizacion = Problema()
        optimizacion.nombre_arch_salida = entradas[num][0:-4]+"_sol.txt"
        num+=1
        #try:
        cargar_problema(optimizacion,i)
        print(optimizacion.toString())
        if(optimizacion.metodo == 0):
            temp_parametros = optimizacion.dar_parametros_Simplex()
            metodoSimplex(temp_parametros[0], temp_parametros[1],optimizacion.nombre_arch_salida)

        elif(optimizacion.metodo == 1):
            temp_parametros = optimizacion.dar_parametros_granM()
            parametro1 = temp_parametros[0]
            parametro2 = temp_parametros[1]
            metodoGranM(parametro1, parametro2,optimizacion.nombre_arch_salida)
            pass
        elif(optimizacion.metodo == 2):
            temp_parametros = optimizacion.dar_parametros_Simplex()
            metodoDosFases(temp_parametros[0], temp_parametros[1],optimizacion.nombre_arch_salida)

        elif(optimizacion.metodo == 3):
            temp_parametros = optimizacion.dar_parametros_dual()
            parametro1 = temp_parametros[0]
            parametro2 = temp_parametros[1]
            parametro3 = optimizacion.tipo_optimizacion
            #Aca va la funcion de Luis y esos son los parametros
            dual.metodo_dual(parametro1,parametro2,parametro3)
            pass
        else:
            print("Esto casi nunca pasa :o")
        #except:
        #    print("Error de formato")
        #finally:
        #    pass






    




    


    

   


