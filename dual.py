'''
¿Como ejecutar este archivo?
metodo_dual("2,3",["4,8,<=,12","2,1,<=,3","3,2,<=,4"],"max")
'''

import simplex as simplex


#Función para generar los datos para pasarlos al método de 2 fases
def datos_dosfases(opera,objetivo):
    lista_salida = [] #arreglo con los datos de salida
    lista_aux = objetivo.split(",")
    tam = len(lista_aux)
    lista_salida.append(str(tam))#número de elementos en la función objetivo
    lista_salida.append(opera)#operación a realizar (max o min)
    lista_salida.append(objetivo)#función objetivo del método dual
    return lista_salida


#Función para generar la objetivo para el dos fases
def obtener_objetivo(matriz):
    tam = len(matriz)
    objetivo = matriz[tam-1]#va a la última posición de la matriz donde esta la función objetivo.
    salida = ""
    contador = 0; 
    for i in objetivo:
        if(i!=0.0): #para eliminar el 0.0 del final, no es parte de la funcion objetivo
            salida+=str(i)
            if(contador!=len(objetivo)-2):#para no agregar la , última.
                salida+=","
            contador+=1
    return salida


#Función para obtener la matriz de restricciones para el método dos fases
def obtener_mat(matriz,operacion):
    mat_salida = []
    if(operacion == "max"):#En caso de ser un problema de maximización
        for i in range(len(matriz)-1):
            fila = []
            contador = 1
            tam_fila = len(matriz[i])
            for j in range(tam_fila):
                if(contador == tam_fila):
                    fila.append(">=")#todos los casos de max van a ser >=
                    fila.append(str(matriz[i][j]))
                else:
                    fila.append(str(matriz[i][j]))
                contador+=1
            mat_salida.append(fila)
    else:#en caso de ser de minimización
        for i in range(len(matriz)-1):
            fila = []
            contador = 1
            tam_fila = len(matriz[i])
            for j in range(tam_fila):
                if(contador == tam_fila):
                    fila.append("<=")#todos los casos de min van a ser <=
                    fila.append(str(matriz[i][j]))
                else:
                    fila.append(str(matriz[i][j]))
                contador+=1
            mat_salida.append(fila)
    return mat_salida
    

#funcion para crear la matriz primal del método dual
def crear_matriz(primal):
    mat_primal_aux = [] 
    for i in range(len(primal)): 
        lista_aux = []  
        for j in range(len(primal[i])):
            try:#intenta convertir las entradas a flotantes, desde un estado de string
                numero = float(primal[i][j])
                lista_aux.append(numero)
            except:#falla en caso de no serlo y lo maneja.
                pass
        mat_primal_aux.append(lista_aux)
    return mat_primal_aux #retorna una matriz creada con los valores de las restricciones.


#función para transpones una matriz de dimensiones a x b
def transponer_matriz(matriz):
    transpuesta=[]

    for i in range(len(matriz[0])):
        colum=[]
        for j in range(len(matriz)):
            temp=matriz[j][i]
            colum.append(temp)
        transpuesta.append(colum)

    return transpuesta


#Función principal del método dual
def metodo_dual(encabezado,restricciones,operacion):
    encab_primal = encabezado.split(",") #remover las , de la función objetivo primal
    encab_primal.append('0') #agregar el 0 para igualar la cantidad de datos 

    restr_primal = []
    for i in restricciones:
        restr_primal.append(i.split(",")) #remover la , de las listas para la matriz primal de restricciones
    mat_primal = crear_matriz(restr_primal) #obtener la matriz primal

    nueva_lista = []
    for i in encab_primal:
        nueva_lista.append(float(i)) #obtener una lista con la función objetivo lista para añadir a la matriz de restricciones
    mat_primal.append(nueva_lista)
    mat_primal_trans = transponer_matriz(mat_primal) #transponer la matriz de restricciones con la función objetivo primal
    
    f_objetivo = obtener_objetivo(mat_primal_trans) #obtener la función objetivo del método dual

    if operacion == "max":
        mat_primal_final = obtener_mat(mat_primal_trans,"min") #generar la matriz primal del método dual
        simplex.metodoDosFases(datos_dosfases("min",f_objetivo),mat_primal_final) #resolver por medio del método de dos fases 
    else:
        mat_primal_final = obtener_mat(mat_primal_trans,"max") #generar la matriz primal del método dual
        simplex.metodoDosFases(datos_dosfases("max",f_objetivo),mat_primal_final) #resolver por medio del método de dos fases 

    return 0