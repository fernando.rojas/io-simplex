# IO-Simplex

El objetivo es demostrar la comprensión del algoritmo del Método Simplex para resolver problemas de maximización y minimización de programación lineal.

# Ejecucion

```bash
python main.py [-h] archivo.txt [archivo2.txt archivo3.txt ...]
```

```bash
python main.py p6.txt
```

# Dependencias
- Pendientes

# Formato de entrada
método, optimización, Número de variables de decisión, Número de restricciones
coeficientes de la función objetivo
coeficientes de las restricciones y signo de restricción


(elementos separados por coma)

Donde método es un valor numérico [ 0=Simplex, 1=GranM, 2=DosFases, 3=Dual ]
Y optimización se indica con min o max

# Desarrolladores
- José Rodolfo Godínez
- José Alexis Torres
- Luis Antonio Arias
- Fernando Rojas